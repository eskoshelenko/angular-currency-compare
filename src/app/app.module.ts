import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CurrencyConvertorComponent } from './currency-convertor/currency-convertor.component';
import { CurrenciesService } from './currencies.service';
import { FixerIoService } from './fixer-io.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CurrencyConvertorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [ CurrenciesService, FixerIoService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
