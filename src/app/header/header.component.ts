import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CurrenciesService } from '../currencies.service';
import { IRate } from '../interfaces';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  subscribers: Subscription[] = []
  rates: IRate[] = []
  base: string = ''
  date: Date = new Date()

  constructor(private currenciesService: CurrenciesService) {}
  
  ngOnInit(): void {
    this.base = this.currenciesService.base()
    const date = this.currenciesService.manager.date
      .subscribe((date) => this.date = date)
    const rates = this.currenciesService.manager.rates
      .subscribe((rates) => this.rates = rates)

    this.subscribers.push(date, rates)
  }

  ngOnDestroy(): void {
    this.subscribers.forEach(subscriber => subscriber.unsubscribe())
  }
}
