import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';

import { interval, throttleTime, catchError, map, throwError, mergeMap, of } from "rxjs";
import { CurrenciesService } from "./currencies.service";
import { IFixerIoData, IRate } from "./interfaces";

@Injectable({
  providedIn: 'root'
})
export class FixerIoService {
  private baseUrl = 'https://api.apilayer.com/fixer'
  private apikey = 'XdxpBwuV7MqpF8RRqdZsEYz3F7zrhfBs'
  constructor(
    private http: HttpClient,
    private currenciesService: CurrenciesService
  ) {}

  private modifyRates(rates: Object): IRate[] {
    return Object.entries(rates)
    .map(([name, value]) => ({ name, value }))
  }

  latestCurrency() {
    const currencies = this.currenciesService.currencies.join(',')
    const refreshTime = this.currenciesService.refreshTime
    const base = this.currenciesService.base()
    const headers = new HttpHeaders({
      apikey: this.apikey
    })

    const interval$ = interval(Infinity)
    const getCurrencies$ = this.http.get(
      `${this.baseUrl}/latest?symbols=${currencies}&base=${base}`, 
      { headers }
    )
    const observable$ = of({
      date: '2022-07-17', 
      base: 'UAH',
      timestamp: Date.now(),
      success: true,
      rates: {
        'UAH': 1,
        'USD': 0.033554684 + Math.random() / 1000,
        'EUR': 0.038168168 + Math.random() / 1000,
      }
    })
    
    return interval$
      .pipe(
        throttleTime(refreshTime),
        mergeMap((number) => observable$),
        catchError((error) => throwError(error)),
        map(data => <IFixerIoData>data),
        map(data => ({...data, rates: this.modifyRates(data.rates)}))
      )
  }
}
