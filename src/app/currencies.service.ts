import { EventEmitter, Injectable } from '@angular/core';
import { IRate, ICalcObject } from './interfaces';

@Injectable({
  providedIn: 'root'
})
export class CurrenciesService {
  manager = {
    date: new EventEmitter<Date>(),
    rates: new EventEmitter<IRate[]>(),
    amount: new EventEmitter<number>(),
    target: new EventEmitter<number>(),
  }
  refreshTime = 30 * 60 * 1000
  currencies = [ 'UAH', 'USD', 'EUR' ]
  rates: IRate[] = []
  roundTo = 5
  
  base(): string {
    return this.currencies[0]
  }

  setRates(rates: IRate[]) {
    this.rates = rates
  }

  getRates(): IRate[] {
    return this.rates
  }

  sendRates(rates: IRate[]) {
    this.manager.rates.emit(rates)
  }

  sendDate(date: Date) {
    this.manager.date.emit(date)
  }

  sendCalcObject(calcObject: ICalcObject) {
    const { from, to, value, type } = calcObject
    this.manager[type].emit(this.calcRate(from, to, value))
  }

  calcRate(from: string, to: string, value: number): number {
    const current = this.rates.find(({ name }) => name === from)!.value || 1
    const target = this.rates.find(({ name }) => name === to)!.value || 1
    const result = this.rates.length ? value * target / current : 0
    return Math.round(result * 10 ** this.roundTo) / 10 ** this.roundTo 
  }
}
