import { EventEmitter } from "@angular/core"

interface IRate {
  name: string, value:number
}

interface ICurrencyManager {
  date: EventEmitter<string>
  rates: EventEmitter<IRate[]>
  amount: EventEmitter<number>
  target: EventEmitter<number>
}

interface ICalcObject {
  type: "amount" | "target"
  from: string
  to: string
  value: number
}

interface IFixerIoData {
  base: string
  date: string
  timestamp: number
  rates: {[key:string]: number}
  success: boolean
}