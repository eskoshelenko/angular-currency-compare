import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CurrenciesService } from '../currencies.service';
import { FixerIoService } from '../fixer-io.service';

@Component({
  selector: 'app-currency-convertor',
  templateUrl: './currency-convertor.component.html',
  styleUrls: ['./currency-convertor.component.css'],
})
export class CurrencyConvertorComponent implements OnInit, OnDestroy {
  subscribers: Subscription[] = []
  base!: string
  currencies: string[] = []
  from!: string
  to!: string
  amount = 1
  target = 1
  lastAction = "from"

  constructor (
    private currenciesService: CurrenciesService,
    private fixerIoService: FixerIoService
  ) {}

  ngOnInit(): void {
    this.base = this.currenciesService.base()
    this.currencies = this.currenciesService.currencies
    this.from = this.currenciesService.base()
    this.to = this.currenciesService.base()
    const amount = this.currenciesService.manager.amount
      .subscribe((amount) => this.amount = amount)
    const target = this.currenciesService.manager.target
      .subscribe((target) => this.target = target)
    const http = this.fixerIoService
      .latestCurrency()
      .subscribe({
        next: ({ rates }) => {
          this.currenciesService.sendDate(new Date())
          this.currenciesService.sendRates(rates)
          this.currenciesService.setRates(rates)
          if (this.lastAction === "from") {
            this.currenciesService.sendCalcObject({
              type: 'target',
              from: this.from,
              to: this.to,
              value: this.amount
            })
          } else {
            this.currenciesService.sendCalcObject({
              type: 'amount',
              from: this.to,
              to: this.from,
              value: this.target
            })
          }
        },
        error: (error) => console.log(error.message),
        complete: () => console.log('Observer completed!')
      })
    this.subscribers.push(amount, target, http)
  }

  ngOnDestroy(): void {
    this.subscribers.forEach(subscriber => subscriber.unsubscribe())
  }

  onSelectFrom(event: Event) {
    this.lastAction = "to"
    this.from = (<HTMLInputElement>event.target).value
    this.currenciesService.sendCalcObject({
      type: 'amount',
      from: this.to,
      to: this.from,
      value: this.target
    })
  }

  onChangeInputTarget(event: Event) {
    this.lastAction = "to"
    this.currenciesService.sendCalcObject({
      type: 'amount',
      from: this.to,
      to: this.from,
      value: this.target
    })
  }

  onSelectTo(event: Event) {
    this.lastAction = "from"
    this.to = (<HTMLInputElement>event.target).value
    this.currenciesService.sendCalcObject({
      type: 'target',
      from: this.from,
      to: this.to,
      value: this.amount
    })
  }

  onChangeInputAmount(event: Event) {
    this.lastAction = "from"
    this.currenciesService.sendCalcObject({
      type: 'target',
      from: this.from,
      to: this.to,
      value: this.amount
    })
  }
}
